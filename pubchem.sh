#!/bin/bash
# Based on https://pubchem.ncbi.nlm.nih.gov/docs/pug-rest

if [ -z "$1" ]; then
    echo "Input file does not specified"
    exit 1
fi

IFS=$'\n'
INPUT_FILE=$1
OUTPUT_FILE=output.csv
NUMBER_OF_LINES=$(wc -l $INPUT_FILE | awk '{print $1}')
LINE_COUNTER=1

echo "\"NAME\",\"PUBCHEM NAME\",\"MOLECULAR FORMULA\",\"MOLECULAR WEIGHT\"" > $OUTPUT_FILE
for COMPOUND in $(cat $INPUT_FILE); do
    echo "$LINE_COUNTER / $NUMBER_OF_LINES"
    PUBCHEM_FIELDS=$(wget -q -O - "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/name/$COMPOUND/property/Title,MolecularFormula,MolecularWeight/json" | jq -r '.PropertyTable.Properties[] | ",\"\(.Title)\",\"\(.MolecularFormula)\",\"\(.MolecularWeight)\""')
    echo "\"$COMPOUND\"$PUBCHEM_FIELDS" >> $OUTPUT_FILE
    (( LINE_COUNTER += 1 ))
done
